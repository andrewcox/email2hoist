# What

Email data to hoist with your very own twisted SMTP server

# Why

I mentioned in jest to the Hoist Product Manager that there should be a way that you can email data to Hoist, but my mind would not let it rest.  

So I bring to you a not so finished method of sending an email to get data into Hoist.

# How

local install (assuming you have the requirement twisted installed)
      
	        git clone https://andrewcox@bitbucket.org/andrewcox/hoist-py.git hoistapi
            git clone https://andrewcox@bitbucket.org/andrewcox/email2hoist.git
            cd email2hoist
            twistd -n -y hoistserver.tac

or provision on AWS using my provisioning script https://bitbucket.org/andrewcox/provisiontwistedserver and the following configuration 

        :::python
        	#START SCRIPT
            import provision
            config = {
                "region" : "us-east-1",
                "image_id" : "ami-51792c38",
                "instance_type" : "t1.micro",
				#You should change these ones
                "security_groups" : ["echo-server-1"],
                "key_name" : "echoserver",
                "key_location" : "echoserver.pem",
				#end changes
                "sc_output_location" : "email2hoist",
                "sc_command" : "git clone https://andrewcox@bitbucket.org/andrewcox/email2hoist.git",
                "twistd_command" : "twistd -o -y hoistserver.tac && sleep 5",
                "twistd_proof_of_life" : "tail twistd.pid",
                "ex_commands": [ "git clone https://andrewcox@bitbucket.org/andrewcox/hoist-py.git hoistapi"  ],
                "svn_not_required":True
            }
            provision.launch_and_run(config)
            #END SCRIPT
			
then send emails in the following form:

	To: <api_key>@<server> 
	From: whereever
	Subject: <data_type>
	
	<The JSON as plain text>




