#Python BulitIns
import os, time, logging
from email.Header import Header
import email, json, sys

#External Modules
from twisted.mail import smtp, maildir
from twisted.web import server
from zope.interface import implements
from twisted.internet import protocol, reactor, defer, task

#add hoistapi to path and load
sys.path.append('../hoistapi')
import hoistapi

class HoistMessage(object): 
    implements(smtp.IMessage)

    def __init__(self, hoist):
        self.logger = logging.getLogger("BaseMessage")
        self.lines = []
        self.FROM = ""
        self.hoist = hoist       
                    
    def lineReceived(self, line):
        self.logger.debug("lineRecieved")
        if line.lower().startswith("from:"):
            self.FROM = line.lower().replace("from:", "").strip()
        self.lines.append(line)

    def eomReceived(self):
        self.logger.debug("eomReceived")  
        self.logger.debug("self.FROM=%s" % self.FROM)  
        d = defer.Deferred()
        self.standardProcess(d)
        return d

    def standardProcess(self, d):
        msg = email.message_from_string("\n".join(self.lines))
        api_key = msg.get("to", "@").split("@")[0]
        obj_type = msg.get("subject")

        self.logger.debug("api_key: " + api_key)
        self.logger.debug("obj_type: " + obj_type)

        for part in msg.walk():
            if part.get_content_type() == "text/plain":
                try:
                    obj = json.loads(part.get_payload(decode=True))
                    self.logger.debug("Setting obj: " + str(obj))
                    h = hoistapi.Hoist(api_key)
                    objs = h[obj_type]
                    saved_obj = objs.set_object(obj)
                    self.logger.debug(str(saved_obj))
                except Exception as ex:
                    self.logger.error("BAD MOJO %s",ex)
                
        return d.callback("finished")

    def connectionLost(self):
        self.logger.debug("connectionLost")
        del(self.lines)
        self.FROM = ""

class LocalDelivery(object): 
    implements(smtp.IMessageDelivery)

    def __init__(self, hoist):
        self.logger = logging.getLogger("LocalDelivery")
        self.hoist = hoist

    def receivedHeader(self, helo, orgin, recipents):
        myHostname, clientIP = helo
        headerValue = "by %s from %s with ESMTP; %s" % (
            myHostname, clientIP, smtp.rfc822date())
        retval = "Recieved: %s" % Header(headerValue)
        #retval = ""
        self.logger.debug("receivedHeader: helo:%s orgin:%s recipents:%s", helo, orgin, [r.dest for r in recipents] )
        return retval

    def validateTo(self, user):
        self.logger.debug("validateTo: %s", user)
        return lambda: HoistMessage(self.hoist)

    def validateFrom(self, helo, orginAddress):
        self.logger.debug("validateFrom: helo:%s orginAddress:%s", helo, orginAddress)
        return orginAddress

class SMTPFactory(protocol.ServerFactory):

    def __init__(self, delivery):
        self.delivery = delivery
        self.logger = logging.getLogger("SMTPFactory")
    
    def buildProtocol(self, addr):
        self.logger.debug("Setting up protocol")
        smtpProtocol = smtp.SMTP(self.delivery)
        smtpProtocol.factory = self
        return smtpProtocol
