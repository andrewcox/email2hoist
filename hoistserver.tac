import hoistSender
from twisted.application import service, internet
import logging

class HSDaemon(object):
    
    def __init__(self, configPath = ""):
        logger = logging.getLogger("")
        logger.setLevel(logging.DEBUG) 
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        ch.setFormatter(formatter)
        logger.addHandler(ch)
        self.logger = logging.getLogger("HSDaemon")
        self.mailDelivery = hoistSender.LocalDelivery(None)
        self.mailFactory = hoistSender.SMTPFactory(self.mailDelivery)
        self.mailPort = 8001
        self.mailInterface = ""


application = service.Application("hoistSender")

daemon = HSDaemon()

mailService = internet.TCPServer(daemon.mailPort,
                                 daemon.mailFactory,
                                 interface=daemon.mailInterface)
mailService.setName("mailService")
mailService.setServiceParent(application)
